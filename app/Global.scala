import play.api._

object Global extends GlobalSettings {

  override def onStop(app: Application) {
    Logger.info("Application shutdown...")
    models.YaBlog.implicitClient.close()
  }

}