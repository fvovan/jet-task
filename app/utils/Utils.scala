package utils

import java.net.URI

object Utils {

  def countDomains(hostList : Seq[String]): Map[String, Int] = {
     hostList.foldLeft(Map[String, Int]() withDefaultValue 0) { (d, c) => d + (c -> (d(c) + 1)) }
  }

  def parseDomain(link : String) : String = {
    val d = new URI(link).getHost
    d.substring( d.lastIndexOf('.', d.lastIndexOf('.') - 1 ) + 1 )
    //crap
  }

}
