package controllers

import scala.concurrent._
import scala.concurrent.duration._

import play.api._
import play.api.Logger
import play.api.mvc._
import play.api.libs.json._

import utils.Utils._

object Application extends Controller {

  def index = Action {
    Ok("Your new application is ready.")
  }

  def search(query: List[String]) = Action.async {
    import play.api.libs.concurrent.Execution.Implicits.defaultContext

    Logger.debug("search req " + query )

    val req = Future.traverse(query) { word =>
      models.YaBlog.foundLinks(word)
    }

    val allLinks = req map { links =>
       Logger.debug( "links " + links )
       links.reduce( _ ++ _ )
    }

    val allDomains = allLinks.map { links =>
      links.toList.map { parseDomain(_) }
    }

    val res = allDomains.map {
      countDomains(_)
    }

/*  val all = Await.result(res, 20 seconds)
    Logger.debug(" domains " + all ) */

   res.map { r =>
      Ok(Json.prettyPrint(Json.toJson(r)))
    }

  }
}