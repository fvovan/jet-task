package models

import scala.concurrent.Future
import scala.xml.Elem

import play.api.libs.ws._
import play.api.Play.current
import play.api.libs.ws.ning._

import com.ning.http.client.extra.ThrottleRequestFilter
import com.ning.http.client._


object YaBlog {
  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

  val throttleConnections = 10 //max simultaneously ws connections
  val maxLinks            = 10 //max result links from each query
  val wsTimeout           = 5000

  val clientConfig = new DefaultWSClientConfig()
  val throttleBuilder = (new AsyncHttpClientConfig.Builder).addRequestFilter(new ThrottleRequestFilter(throttleConnections))
  val builder = new NingAsyncHttpClientConfigBuilder(clientConfig, throttleBuilder).build()

  implicit val implicitClient = new NingWSClient(builder)

  def requestYandex(query: String) : Future[WSResponse] = {
    WS.clientUrl("https://blogs.yandex.ru/search.rss").
      withQueryString(("text", query)).
      withFollowRedirects(true).
      withHeaders(
        ("User-Agent" , "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0")
      ).
      withRequestTimeout(wsTimeout).get()
  }

  def extractLinks( xml : Elem ) : Set[String] = {
    (for {
      link <- (xml \\ "item" \ "link").view
    }
    yield link.text).
      take(maxLinks).force.toSet
  }

  def foundLinks(query: String) : Future[Set[String]] = {
    requestYandex(query).map { resp =>
      extractLinks(resp.xml)
    }
  }

}
